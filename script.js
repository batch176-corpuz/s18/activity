function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = level * 3;
	this.attack = level;

	this.tackle = (enemy) => {
		enemy.health -= this.attack;
		const log = `${this.name} tackled ${enemy.name} for ${this.attack} damage
        ${enemy.name} health: ${enemy.health}`;
		console.log(log);
		enemy.health < 5 ? enemy.faint() : 0;
	};
	this.faint = () => console.log(`Oh no! ${this.name} fainted.`);
}

const pikachu = new Pokemon("Pikachu", 5);
const bulbasaur = new Pokemon("Bulbasaur", 7);

pikachu.tackle(bulbasaur);
pikachu.tackle(bulbasaur);
pikachu.tackle(bulbasaur);
pikachu.tackle(bulbasaur);
